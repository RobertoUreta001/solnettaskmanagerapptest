# SolnetTaskManagerAppTest


Bugs:
Bug001 When check box is clicked, no tick appears within the checkbox, but task is marked as completed with Strikethru over text.
Bug002 newly added tasks which are flagged as important do not appear listed within the "Important Tasks" Tab
BUG003 existing task "Test Task 1" does not display the task due date under "My day" tab
Bug004 after a new task is added, The "task title *" field is displayed in red which gives the appearance that there is an error in the screen even though the user has not made an effort to load a new task
Bug005 same day task added, if same day task flagged as important, the task does not appear inside "Important Tasks" tab
